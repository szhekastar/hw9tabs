

const tabsList = document.querySelector('.tabs');
const tabsText = document.querySelectorAll('.tabs-content li');

tabsList.addEventListener('click', (event)=> {
    const target = event.target;
    const index = target.getAttribute('data-li');
    target.closest('ul').querySelector('.active').classList.remove('active');
    target.classList.add('active');
    
    tabsText.forEach((item) => {
        const tabsId = item.getAttribute('data-li');
        if (index !== tabsId) {
            item.classList.add('tabs-text');
        }else{
            item.classList.remove('tabs-text');
        }
    });

} );
